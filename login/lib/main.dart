import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:login/app_state.dart';
import 'bloc/simple_bloc_delegate.dart';

main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(App());
}
