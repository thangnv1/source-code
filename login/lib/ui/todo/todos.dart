import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:login/model/todos.dart';

class TodoScreen extends StatefulWidget {
  final String userId;
  const TodoScreen({Key key, this.userId}) : super(key: key);
  @override
  State<StatefulWidget> createState() => new _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  List<Todos> _todoList;
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final _textEditingController = TextEditingController();
  StreamSubscription<Event> _onTodosAddSub;
  StreamSubscription<Event> _onTodosChangeSub;
  Query _todoQuery;

  @override
  void initState() {
    super.initState();
    _todoList = new List();
    _todoQuery = _database
        .reference()
        .child("todo")
        .orderByChild("userId")
        .equalTo(widget.userId);
    _onTodosAddSub = _todoQuery.onChildAdded.listen(_onEntryAdded);
    _onTodosChangeSub = _todoQuery.onChildChanged.listen(_onEntryChanged);
  }

  @override
  void dispose() {
    _onTodosAddSub.cancel();
    _onTodosChangeSub.cancel();
    super.dispose();
  }

  _onEntryAdded(Event event) {
    setState(() {
      _todoList.add(Todos.fromSnapshot(event.snapshot));
    });
  }

  void _onEntryChanged(Event event) {
    var oldEntry = _todoList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      _todoList[_todoList.indexOf(oldEntry)] =
          Todos.fromSnapshot(event.snapshot);
    });
  }

  _addNewTodo(String todoItem) {
    if (todoItem.length > 0) {
      Todos todo = new Todos(todoItem.toString(), widget.userId, false);
      _database.reference().child("todo").push().set(todo.toJson());
    }
  }

  _updateTodo(Todos todo) {
    todo.isCompleted = !todo.isCompleted;
    if (todo != null) {
      _database.reference().child("todo").child(todo.key).set(todo.toJson());
    }
  }

  _deleteTodo(String todoId, int index) {
    _database.reference().child("todo").child(todoId).remove().then((_) {
      setState(() {
        _todoList.removeAt(index);
      });
    });
  }

  _showDialog(BuildContext context) async {
    _textEditingController.clear();
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Row(
              children: <Widget>[
                new Expanded(
                    child: new TextFormField(
                  controller: _textEditingController,
                  decoration: new InputDecoration(
                    labelText: 'Add new todo',
                  ),
                ))
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Save'),
                  onPressed: () {
                    _addNewTodo(_textEditingController.text.toString());

                    Navigator.pop(context);
                  })
            ],
          );
        });
  }

  Widget _showTodoList() {
    if (_todoList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: _todoList.length,
          itemBuilder: (BuildContext context, int index) {
            String todoId = _todoList[index].key;
            String subject = _todoList[index].subject;
            bool completed = _todoList[index].isCompleted;
            return Dismissible(
              key: Key(todoId),
              background: Container(color: Colors.red),
              onDismissed: (direction) async {
                _deleteTodo(todoId, index);
              },
              child: ListTile(
                title: Text(
                  subject,
                  style: TextStyle(fontSize: 20.0),
                ),
                trailing: IconButton(
                    icon: (completed)
                        ? Icon(
                            Icons.done_outline,
                            color: Colors.green,
                            size: 20.0,
                          )
                        : Icon(Icons.done, color: Colors.grey, size: 20.0),
                    onPressed: () {
                      _updateTodo(_todoList[index]);
                    }),
              ),
            );
          });
    } else {
      return Center(
          child: Text(
        "Your list is empty",
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
            title: new Center(
          child: Text('Todos Data List'),
        )),
        body: _showTodoList(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _showDialog(context);
          },
          child: Icon(Icons.add),
        ));
  }
}
