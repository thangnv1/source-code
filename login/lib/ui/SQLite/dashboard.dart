import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:login/ui/SQLite/user.dart';

import 'add_user.dart';
import 'firebase_database.dart';

class UserDashboard extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<UserDashboard> implements AddUserCallback {
  //bool _anchorToBottom = false;
  FirebaseDatabaseUtil databaseUtil;
  @override
  void initState() {
    super.initState();
    databaseUtil = new FirebaseDatabaseUtil();
    databaseUtil.initState();
  }

  @override
  void dispose() {
    super.dispose();
    databaseUtil.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text("Query Firebase DB"),
        centerTitle: true,
      ),
      body: new FirebaseAnimatedList(
        itemBuilder: (BuildContext context, DataSnapshot snapshot,
            Animation<double> animation, int index) {
          return new SizeTransition(
            sizeFactor: animation,
            child: showUser(snapshot),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => showEditWidget(null, false),
      ),
    );
  }

  @override
  void addUser(User user) {
    setState(() {
      databaseUtil.addUser(user);
    });
  }

  @override
  void update(User user) {
    setState(() {
      databaseUtil.updateUser(user);
    });
  }

  Widget showUser(DataSnapshot res) {
    User user = User.fromSnapshot(res);
    var item = new Card(
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Padding(
              padding: EdgeInsets.all(10.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    "name: " + user.name,
                    style: new TextStyle(
                        fontSize: 20.0, color: Colors.lightBlueAccent),
                  ),
                  new Text(
                    "email: " + user.email,
                    style: new TextStyle(fontSize: 20.0, color: Colors.white),
                  ),
                  new Text(
                    "age: " + user.age,
                    style: new TextStyle(fontSize: 20.0, color: Colors.yellow),
                  ),
                  new Text(
                    "mobile no: " + user.mobile,
                    style: new TextStyle(fontSize: 20.0, color: Colors.amber),
                  ),
                ],
              ),
            ),
          ),
          new Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new IconButton(
                icon: const Icon(
                  Icons.edit,
                ),
                onPressed: () => showEditWidget(user, true),
              ),
              new IconButton(
                icon: const Icon(Icons.delete_forever),
                onPressed: () => deleteUser(user),
              ),
            ],
          ),
        ],
      ),
    );
    return item;
  }

  showEditWidget(User user, bool isEdit) {
    showDialog(
      context: context,
      builder: (BuildContext context) =>
          new AddUserDialog().buildAboutDialog(context, this, isEdit, user),
    );
  }

  deleteUser(User user) {
    setState(() {
      databaseUtil.deleteUser(user);
    });
  }
}
