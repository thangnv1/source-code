import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthenticationState extends Equatable {
  AuthenticationState([List props = const []]) : super(props);
}

class Uninitialized extends AuthenticationState {
}

class Authenticated extends AuthenticationState {
  final String displayName;
  Authenticated(this.displayName) : super([displayName]);
}

class Unauthenticated extends AuthenticationState {
}