import 'package:firebase_database/firebase_database.dart';

class Todos {
  String key;
  String subject;
  bool isCompleted;
  String userId;

  Todos(this.subject, this.userId, this.isCompleted);

  Todos.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        userId = snapshot.value["userId"],
        subject = snapshot.value["subject"],
        isCompleted = snapshot.value["isCompleted"];
  toJson() {
    return {
      "userId": userId,
      "subject": subject,
      "isCompleted": isCompleted,
    };
  }
}
