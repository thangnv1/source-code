import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class PickAndShow extends StatefulWidget {
  PickAndShow() : super();
  @override
  _PickAndShowState createState() => _PickAndShowState();
}

class _PickAndShowState extends State<PickAndShow> {
  Future<File> imageFile;
  pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
  }

  Widget showImage() {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        Size size = MediaQuery.of(context).size;
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: size.width,
            height: size.height,
          );
        } else {
          return Center(
            child: const Text(
              'No Image Selected',
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: new Center(
        child: Text(
          'Pick Image',
          textAlign: TextAlign.center,
        ),
      )),
      body: showImage(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
        onPressed: () {
          pickImageFromGallery(ImageSource.gallery);
        },
      ),
    );
  }
}
